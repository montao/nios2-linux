/* This is a generated file, don't edit */

#define NUM_APPLETS 125

const char applet_names[] ALIGN1 = ""
"[" "\0"
"[[" "\0"
"awk" "\0"
"base64" "\0"
"basename" "\0"
"blockdev" "\0"
"bootchartd" "\0"
"bunzip2" "\0"
"bzcat" "\0"
"cat" "\0"
"chgrp" "\0"
"chmod" "\0"
"chown" "\0"
"chroot" "\0"
"clear" "\0"
"cmp" "\0"
"cp" "\0"
"cpio" "\0"
"cut" "\0"
"date" "\0"
"dd" "\0"
"df" "\0"
"diff" "\0"
"dmesg" "\0"
"dnsdomainname" "\0"
"du" "\0"
"echo" "\0"
"env" "\0"
"expr" "\0"
"false" "\0"
"fgconsole" "\0"
"find" "\0"
"flock" "\0"
"free" "\0"
"ftpget" "\0"
"ftpput" "\0"
"getopt" "\0"
"getty" "\0"
"grep" "\0"
"gunzip" "\0"
"gzip" "\0"
"head" "\0"
"hexdump" "\0"
"hostname" "\0"
"hush" "\0"
"hwclock" "\0"
"id" "\0"
"ifconfig" "\0"
"insmod" "\0"
"iostat" "\0"
"kill" "\0"
"killall" "\0"
"last" "\0"
"ln" "\0"
"logname" "\0"
"losetup" "\0"
"ls" "\0"
"lsmod" "\0"
"md5sum" "\0"
"mkdir" "\0"
"mkfifo" "\0"
"mknod" "\0"
"modinfo" "\0"
"modprobe" "\0"
"more" "\0"
"mount" "\0"
"mpstat" "\0"
"mv" "\0"
"nbd-client" "\0"
"nc" "\0"
"netstat" "\0"
"nslookup" "\0"
"passwd" "\0"
"pidof" "\0"
"ping" "\0"
"pmap" "\0"
"powertop" "\0"
"printf" "\0"
"ps" "\0"
"pwd" "\0"
"readlink" "\0"
"renice" "\0"
"rev" "\0"
"rm" "\0"
"rmdir" "\0"
"rmmod" "\0"
"route" "\0"
"sed" "\0"
"seq" "\0"
"sh" "\0"
"sleep" "\0"
"smemcap" "\0"
"sort" "\0"
"stty" "\0"
"sync" "\0"
"tail" "\0"
"tar" "\0"
"tee" "\0"
"test" "\0"
"tftp" "\0"
"time" "\0"
"top" "\0"
"touch" "\0"
"tr" "\0"
"traceroute" "\0"
"true" "\0"
"tty" "\0"
"umount" "\0"
"uname" "\0"
"uniq" "\0"
"unxz" "\0"
"uptime" "\0"
"usleep" "\0"
"vconfig" "\0"
"vi" "\0"
"watch" "\0"
"wc" "\0"
"wget" "\0"
"which" "\0"
"whoami" "\0"
"xargs" "\0"
"xz" "\0"
"xzcat" "\0"
"yes" "\0"
"zcat" "\0"
;

#ifndef SKIP_applet_main
int (*const applet_main[])(int argc, char **argv) = {
test_main,
test_main,
awk_main,
base64_main,
basename_main,
blockdev_main,
bootchartd_main,
bunzip2_main,
bunzip2_main,
cat_main,
chgrp_main,
chmod_main,
chown_main,
chroot_main,
clear_main,
cmp_main,
cp_main,
cpio_main,
cut_main,
date_main,
dd_main,
df_main,
diff_main,
dmesg_main,
hostname_main,
du_main,
echo_main,
env_main,
expr_main,
false_main,
fgconsole_main,
find_main,
flock_main,
free_main,
ftpgetput_main,
ftpgetput_main,
getopt_main,
getty_main,
grep_main,
gunzip_main,
gzip_main,
head_main,
hexdump_main,
hostname_main,
hush_main,
hwclock_main,
id_main,
ifconfig_main,
insmod_main,
iostat_main,
kill_main,
kill_main,
last_main,
ln_main,
logname_main,
losetup_main,
ls_main,
lsmod_main,
md5_sha1_sum_main,
mkdir_main,
mkfifo_main,
mknod_main,
modinfo_main,
modprobe_main,
more_main,
mount_main,
mpstat_main,
mv_main,
nbdclient_main,
nc_main,
netstat_main,
nslookup_main,
passwd_main,
pidof_main,
ping_main,
pmap_main,
powertop_main,
printf_main,
ps_main,
pwd_main,
readlink_main,
renice_main,
rev_main,
rm_main,
rmdir_main,
rmmod_main,
route_main,
sed_main,
seq_main,
hush_main,
sleep_main,
smemcap_main,
sort_main,
stty_main,
sync_main,
tail_main,
tar_main,
tee_main,
test_main,
tftp_main,
time_main,
top_main,
touch_main,
tr_main,
traceroute_main,
true_main,
tty_main,
umount_main,
uname_main,
uniq_main,
unxz_main,
uptime_main,
usleep_main,
vconfig_main,
vi_main,
watch_main,
wc_main,
wget_main,
which_main,
whoami_main,
xargs_main,
unxz_main,
unxz_main,
yes_main,
gunzip_main,
};
#endif

const uint16_t applet_nameofs[] ALIGN2 = {
0x0000,
0x0002,
0x0005,
0x0009,
0x0010,
0x0019,
0x0022,
0x002d,
0x0035,
0x003b,
0x003f,
0x0045,
0x004b,
0x0051,
0x0058,
0x005e,
0x0062,
0x0065,
0x006a,
0x006e,
0x0073,
0x0076,
0x0079,
0x007e,
0x0084,
0x0092,
0x0095,
0x009a,
0x009e,
0x00a3,
0x00a9,
0x00b3,
0x00b8,
0x00be,
0x00c3,
0x00ca,
0x00d1,
0x00d8,
0x00de,
0x00e3,
0x00ea,
0x00ef,
0x00f4,
0x00fc,
0x0105,
0x010a,
0x0112,
0x0115,
0x011e,
0x0125,
0x012c,
0x0131,
0x0139,
0x013e,
0x0141,
0x0149,
0x0151,
0x0154,
0x015a,
0x0161,
0x0167,
0x016e,
0x0174,
0x017c,
0x0185,
0x018a,
0x0190,
0x0197,
0x019a,
0x01a5,
0x01a8,
0x01b0,
0x81b9,
0x01c0,
0x41c6,
0x01cb,
0x01d0,
0x01d9,
0x01e0,
0x01e3,
0x01e7,
0x01f0,
0x01f7,
0x01fb,
0x01fe,
0x0204,
0x020a,
0x0210,
0x0214,
0x0218,
0x021b,
0x0221,
0x0229,
0x022e,
0x0233,
0x0238,
0x023d,
0x0241,
0x0245,
0x024a,
0x024f,
0x0254,
0x0258,
0x025e,
0x4261,
0x026c,
0x0271,
0x0275,
0x027c,
0x0282,
0x0287,
0x028c,
0x0293,
0x029a,
0x02a2,
0x02a5,
0x02ab,
0x02ae,
0x02b3,
0x02b9,
0x02c0,
0x02c6,
0x02c9,
0x02cf,
0x02d3,
};


#define MAX_APPLET_NAME_LEN 13
