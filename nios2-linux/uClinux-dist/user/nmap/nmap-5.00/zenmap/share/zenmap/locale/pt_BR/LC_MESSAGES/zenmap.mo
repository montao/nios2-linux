��    ]           �      �     �     �  	     	             '     3     :     G     U     ]     j     s     |  "   �     �     �     �     �     �     �     	     )	     6	     C	     P	  	   \	     f	     r	  	   �	     �	     �	     �	     �	  
   �	     �	     �	     �	     �	     �	     �	     �	     
     
     $
  	   -
     7
  #   E
     i
     z
  
   �
     �
     �
     �
     �
     �
  	   �
  	   �
     �
  	   	  
             -     6  	   I     S     n     �     �     �     �     �     �     �     �     �     �     �                         %  
   6  )   A  b   k     �     �     �  
   �     �  
     3       E  	   Z  
   d  
   o  #   z     �     �     �     �     �     �     �            ,   (     U     f     v     �     �  '   �     �     �     �     	          1     E     [     q     �     �     �     �     �     �     �     �     �     �  '   �     #     1  !   ?     a     n     }  2   �     �     �     �     �               )     8  
   @     K  #   \     �     �     �     �  #   �  
   �     �          4     Q     ^     f     u     �  
   �     �     �     �     �     �     �  
   �     �     �       ,   !  _   N     �     �     �     �     �     �        E          M   D   J           "      -   ?      6      7   .   #   (   *   U           W   A   Z      [   H   9       T   )          K   Q             5       V         &   '                       
   P   L           ;       I   +       B   N   O              R   2   4   !   >   3             %          :   ]      \      /      C   F   Y                        <      8   $                                  0   S       1               G   	       X   =          ,   @    # scanned ports: Accuracy Accuracy: Addresses Can't save file Choose file Class: Close anyway Closed ports: Command Command Info Command: Comments Compare Results Compare Scan Results using Diffies Debug level: Deleting Profile Description Difficulty: Display Order / Grouping Edit selected scan profile Filtered ports: Finished on: General Info Host Details Host Status Hostnames Hosts down: Hosts scanned: Hosts up: IP ID Sequence IPv4: IPv6: Index: Last boot: MAC: Name - Type: Name: Nmap Output Nmap Version: Non root user Not Available Not available Nothing to save OS Class OS Family OS Generation Open the results of a previous scan Operating System Port-Protocol-State: Ports used Profile Profile Editor Profile Information Profile name Profile: Protocol: Save Scan Save current scan results Scan Info Scan type: Scanned ports: Scanning Select Scan Result Services: Shows the application help Sort by port number Sort by service name Started on: State: TCP Sequence TCP TS Sequence Target: Translation Type Unknown Unknown Host Unnamed profile Up time: Values: Vendor Vendor: Verbosity level: Written by You must provide a name for this profile. Your profile is going to be deleted! Click Ok to continue, or Cancel to go back to Profile Editor. _About _Edit Selected Profile _Help _Open Scan _Profile _Save Scan Project-Id-Version: Zenmap
Report-Msgid-Bugs-To: nmap-dev@insecure.org
POT-Creation-Date: 2009-05-08 09:22-0600
PO-Revision-Date: 2006-04-12 11:53-0300
Last-Translator: Adriano Monteiro Marques <py.adriano@gmail.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 # portas analisadas: Precisão Precisão: Endereços Não foi possível salvar o arquivo Escolha um arquivo Classe: Fechar mesmo assim Portas Fechadas: Comando Informações do Comando Comando: Comentários Comparar Resultados Compara resultados de varreduras usando Diff Nível de Debug: Apagando Perfil Descrição Dificuldade: Mostrar Ordem / Agrupamentos Edita o perfil de varredura selecionado Portas Filtradas: Terminado em: Informações Gerais Detalhes da Máquina Status da Máquina Nomes das máquinas Máquinas desligadas: Máquinas analisadas: Máquinas ligadas: Sequência IP ID IPv4: IPv6: Índice: Última inicialização: MAC: Nome - Tipo: Nome: Saída do Nmap Versão do Nmap: Usuário não-root (não-administrador) Indisponível Indisponível Nenhuma modificação a ser salva Classe do SO Família do SO Geração do SO Abre resultados de uma varredura previamente salva Sistema Operacional Porta-Protocolo-Estado: Portas usadas Perfil Editor de Perfil Informação do Perfil Nome do perfil Perfil: Protocolo: Salvar Varredura Salva resultados da varredura atual Informações da Varredura Tipo de Varredura: Portas analisadas: Varrendo Selecione um resultado de varredura Serviços: Mostra a ajuda da aplicação Ordenar por número de porta Ordenar por nome de serviço Iniciado em: Estado: Sequência TCP Sequência TCP TS Alvo: Tradução Tipo Desconhecido Máquina desconhecida Perfil sem nome Tempo ligado: Valores: Fabricante Fabricante: Nível de Verbosidade: Escrito por Você precisa atribuir um nome a este perfil Seu Perfil será apagado! Clique Ok para continuar, ou Cancelar para voltar ao Editor de Perfil _Sobre _Editar Perfil Selecionado Ajuda _Abrir varredura _Perfil _Salvar Varredura 