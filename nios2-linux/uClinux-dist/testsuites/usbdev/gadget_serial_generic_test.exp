#!/usr/bin/expect

#
# This usb device test script is for Gadget Serial dirver on BF548-EZKIT talking with Generic USB Serial Driver on Linux host PC
#

source  ../kernel_config.exp
log_file [log_file_name "$argv0"]
send_user "Starting $argv0\n"
set TITLE [title "$argv0"]

set musb_mode [lindex $argv 1]
send_user "\n#### musb_mode is $musb_mode\n"

if { $argc < 1} {
    puts "Please input: musb_mode(PIO or DMA)"
    exit
}
append TITLE " ($musb_mode)"


step "Spawn kermit"
source ../spawn_kermit.exp

step "Reboot the kernel."
# To add following line here to indicate that if the bootargs is different,
# it could be set here. If the bootargs value is 0, it needn't be stated,
# for it is the default value.
# set bootargs  $bootargs_param0
source ../reboot_kernel.exp

step "Start testing."
set case_num 0

set timeout 5
sleep 3
send "ifconfig eth0 $targetip\r"
sleep 3
send "\r"
expect -re $kernel_prompt

incr case_num
set timeout 10
send "modprobe g_serial use_acm=0\r"
while 1 {
    expect {
        -re "modprobe:|\[fF]ail" {
            case_fail $case_num
        }
        -re "Gadget Serial.*g_serial ready.*speed config.*Generic Serial config" {
            case_pass $case_num
            break
        }
        timeout {
            case_fail $case_num
        }
    }
}

incr case_num
set timeout 10
send "dmesg|tail\r"
while 1 {
    expect {
        -re "Gadget Serial.*g_serial ready.*speed config.*Generic Serial config.*$kernel_prompt" {
            case_pass $case_num
            break
        }
        timeout {
            case_fail $case_num
        }
    }
}

spawn /bin/bash
set host_spawn_id $spawn_id

incr case_num
set timeout 15
send "su\r"
while 1 {
    expect {
        -re "\[pP]assword" {
            send "$password\r"
            continue
        }
        -re "\[eE]rror|\[fF]ail" {
            case_fail $case_num
        }
        -re "#" {
            case_pass $case_num
            break
        }
        timeout {
            case_fail $case_num
        }
    }
}

incr case_num
set timeout 15
send "sudo mount -t usbfs none /proc/bus/usb/\r"
while 1 {
    expect {
        -re "mount: none already mounted" {
            case_pass $case_num
            break
        }
        -re "\[eE]rror|\[fF]ail" {
            case_fail $case_num
        }
        -re "#" {
            case_pass $case_num
            break
        }
        timeout {
            case_fail $case_num
        }
    }
}

incr case_num
set timeout 4
send "mount\r"
while 1 {
    expect {
        -re "none on /proc/bus/usb type usbfs.*#" {
            case_pass $case_num
            break
        }
        timeout {
            case_fail $case_num
        }
    }
}

sleep 2
incr case_num
set timeout 5
send "cat /proc/bus/usb/devices|grep -i gadget\r"
while 1 {
    expect {
        -re "Product=Gadget Serial.* #" {
            case_pass $case_num
            break
        }
        timeout {
            case_fail $case_num
        }
    }
}

incr case_num
set timeout 5
send "sudo modprobe usbserial vendor=0x0525 product=0xA4A6\r"
while 1 {
    expect {
        -nocase -re "modprobe:|error|fail" {
            case_fail $case_num
        }
        -re "#" {
            case_pass $case_num
            break
        }
        timeout {
            case_fail $case_num
        }
    }
}

incr case_num
set timeout 5
send "lsmod |grep usbserial\r"
while 1 {
    expect {
        -nocase -re "lsmod:|error|fail" {
            case_fail $case_num
        }
        -re "\r\nusbserial.*#" {
            case_pass $case_num
            break
        }
        timeout {
            case_fail $case_num
        }
    }
}


set spawn_id $kermit_spawn_id
sleep 3
incr case_num
set timeout 8
send "getty 9600 /dev/ttyGS0\r"
while 1 {
    expect {
        -re $kernel_prompt {
            case_fail $case_num
        }
        timeout {
            case_pass $case_num
            break
        }
    }
}


spawn /bin/bash
set gserial_spawn_id $spawn_id 
incr case_num
set timeout 5
send "kermit configs/zmodem.ttyUSB0\r"
while 1 {
    expect {
        -re "ttyUSB0 >" {
            case_pass $case_num
            break
        }
        timeout {
            case_fail $case_num
        }
    }
}

incr case_num
set timeout 5
send "connect\r"
while 1 {
    expect {
        -re "login:" {
            case_pass $case_num
            break
        }
        timeout {
            case_fail $case_num
        }
    }
}

send "root\r"
expect root

set spawn_id $kermit_spawn_id
expect "Password:"

incr case_num
set timeout 5
send "uClinux\r"
while 1 {
    expect {
        -re $kernel_prompt {
            case_fail $case_num
        }
        timeout {
            case_pass $case_num
            break
        }
    }
}


step "Send file to host PC through gadget serial"
set spawn_id $gserial_spawn_id

incr case_num
set timeout 8
while 1 {
    expect {
        -re $kernel_prompt {
            case_pass $case_num
            break
        }
        timeout {
            case_fail $case_num
        }
    }
}

incr case_num
set timeout 5
send "cp /proc/kallsyms /file_in_target\r"
while 1 {
    expect {
        -re $kernel_prompt {
            case_pass $case_num
            break
        }
        timeout {
            case_fail $case_num
        }
    }
}

incr case_num
set timeout 5
send "lsz /file_in_target\r"
while 1 {
    expect {
        -re "Transfer complete" {
            case_pass $case_num
            break
        }
        timeout {
            case_fail $case_num
        }
    }
}

incr case_num
set timeout 5
send "\r"
while 1 {
    expect {
        -re $kernel_prompt {
            case_pass $case_num
            break
        }
        timeout {
            case_fail $case_num
        }
    }
}

set spawn_id $host_spawn_id

incr case_num
set timeout 5
send "rcp root@$targetip:/file_in_target file_in_target.rcp\r"
while 1 {
    expect {
        -re "\[eE]rror|\[fF]ail|No such" {
            case_pass $case_num
            break
        }
        -re "#" {
            case_pass $case_num
            break
        }
        timeout {
            case_fail $case_num
        }
    }
}

incr case_num
set timeout 5
send "diff -q file_in_target.rcp file_in_target\r"
while 1 {
    expect {
        -nocase -re "(diff:|fail|error|no such|differ).* #" {
            case_fail $case_num
        }
         -re " #" {
            case_pass $case_num
            break
        }
        timeout {
            case_fail $case_num
        }
    }
}


step "Send file to target board through gadget serial"
set spawn_id $gserial_spawn_id

sleep 2
incr case_num
set timeout 5
send "\0c"
while 1 {
    expect {
        -re "ttyUSB0 >" {
            case_pass $case_num
            break
        }
        timeout {
            case_fail $case_num
        }
    }
}

incr case_num
set timeout 5
send "send file_in_target.rcp\r"
while 1 {
    expect {
        -re "Transfer complete.*>" {
            case_pass $case_num
            break
        }
        timeout {
            case_fail $case_num
        }
    }
}

incr case_num
set timeout 5
send "connect\r"
send "\r"
while 1 {
    expect {
        -re $kernel_prompt {
            case_pass $case_num
            break
        }
        timeout {
            case_fail $case_num
        }
    }
}

incr case_num
set timeout 5
send "diff -q /file_in_target file_in_target.rcp\r"
while 1 {
    expect {
        -nocase -re "(diff:|fail|error|no such|differ).*$kernel_prompt" {
            case_fail $case_num
        }
         -re $kernel_prompt {
            case_pass $case_num
            break
        }
        timeout {
            case_fail $case_num
        }
    }
}

incr case_num
set timeout 8
send "modprobe -r g_serial\r"
expect -re $kernel_prompt

incr case_num
set timeout 10
send "\0q\r"
while 1 {
    expect {
        -re "Closing.*OK" {
            case_pass $case_num
            break
        }
        timeout {
            case_fail $case_num
        }
    }
}


set spawn_id $host_spawn_id 
sleep 2

incr case_num
set timeout 5
send "rm file_in_target*\r"
while 1 {
    expect {
        -nocase -re "(rm:|fail|error|no such).* #" {
            case_fail $case_num
        }
         -re " #" {
            case_pass $case_num
            break
        }
        timeout {
            case_fail $case_num
        }
    }
}

incr case_num
set timeout 5
send "umount /proc/bus/usb\r"
while 1 {
    expect {
        -re "umount:|\[eE]rror|\[fF]ail" {
            case_fail $case_num
        }
        -re "#" {
            case_pass $case_num
            break
        }
        timeout {
            case_fail $case_num
        }
    }
}

incr case_num
set timeout 4
send "mount\r"
while 1 {
    expect {
        -re "none on /proc/bus/usb type usbfs.*#" {
            case_fail $case_num
        }
        -re "#" {
            case_pass $case_num
            break
        }
        timeout {
            case_fail $case_num
        }
    }
}

incr case_num
set timeout 5
send "sudo modprobe -r usbserial\r"
while 1 {
    expect {
        -re "modprobe:|\[eE]rror|\[fF]ail" {
            case_fail $case_num
        }
        -re "#" {
            case_pass $case_num
            break
        }
        timeout {
            case_fail $case_num
        }
    }
}

incr case_num
set timeout 5
send "lsmod|grep usb\r"
while 1 {
    expect {
        -re "usbserial.*#" {
            case_fail $case_num
        }
        -re "#" {
            case_pass $case_num
            break
        }
        timeout {
            case_fail $case_num
        }
    }
}

all_pass
send_user "Ending $argv0\n"
log_file
