#!/usr/bin/expect

source  ../kernel_config.exp
log_file [log_file_name "$argv0"]
send_user "Starting $argv0\n"
set TITLE [title "$argv0"]

set musb_mode [lindex $argv 1]
send_user "\n#### musb_mode is $musb_mode\n"

if { $argc < 1} {
    puts "Please input: musb_mode(PIO or DMA)"
    exit
}
append TITLE " ($musb_mode)"


step "Spawn kermit"
source ../spawn_kermit.exp

step "Reboot the kernel."
# To add following line here to indicate that if the bootargs is different,
# it could be set here. If the bootargs value is 0, it needn't be stated,
# for it is the default value.
# set bootargs  $bootargs_param0
source ../reboot_kernel.exp

set wakeup_pattern "Syncing filesystems.*Freezing user space processes \.\.\. \\\(elapsed.*seconds\\\) done\.\r\nFreezing remaining freezable tasks \.\.\. \\\(elapsed.*seconds\\\) done\.\r\nSuspending console\\\(s\\\).*Restarting tasks.*done"

set wait_time 10
set standby_fail 0
set pm_state_str standby
set Total_Count 5

proc standby_test_case {} {
    global case_num
    global kernel_prompt
    global wait_time
    global standby_fail
    global pm_state_str
    global wakeup_pattern
    global kermit_spawn_id
    global host_spawn_id
    global mnt_point
    global dev

    set spawn_id $kermit_spawn_id
    send "\r"
    expect -re $kernel_prompt

    set timeout [expr $wait_time + 10]
    incr case_num
    send "\nrtcwake -s $wait_time -m $pm_state_str\r"
    while 1 {
        expect {
            -re $wakeup_pattern {
                case_pass $case_num
                break
            }
            timeout {
                send_user "Wake up fail\n"
                incr standby_fail
                break
            }
        }
    }

    set timeout 5
    expect -re "Full\r\n|lpa 0x41E1\r\n"

    set timeout 5
    incr case_num
    send "lsmod\r"
    while 1 {
        expect {
            -re "g_file_storage.*$kernel_prompt" {
                case_pass $case_num
                break
            }
            timeout {
                send_user "Fail to execute command after wake up\n"
                case_fail $case_num
            }
        }
    }

    set spawn_id $host_spawn_id
    sleep 3
    send "ls $mnt_point\r"
    expect "#"

    incr case_num
    set timeout 5
    send "mount -t ext2 -o sync $dev $mnt_point\r"
    while 1 {
        expect {
            -re "(mount:|\[eE]rror|\[fF]ail).*#" {
                case_fail $case_num
            }
            -re "#" {
                case_pass $case_num
                break
            }
            timeout {
                case_fail $case_num
            }
        }
    }

    incr case_num
    set timeout 5
    send "ls -l $mnt_point\r"
    while 1 {
        expect {
            -re "(\[eE]rror|\[fF]ail).*#" {
                case_fail $case_num
            }
            -re "10m\.bin.*lost\\+found.*#" {
                case_pass $case_num
                break
            }
            timeout {
                case_fail $case_num
            }
        }
    }

    incr case_num
    set timeout 10
    send "rm $mnt_point/10m.bin\r"
    while 1 {
        expect {
            -re "(\[eE]rror|\[fF]ail).*#" {
                case_fail $case_num
            }
            -re "#" {
                case_pass $case_num
                break
            }
            timeout {
                case_fail $case_num
            }
        }
    }

    incr case_num
    set timeout 5
    send "ls -l $mnt_point/\r"
    while 1 {
        expect {
            -re "(10m\.bin|\[eE]rror|\[fF]ail).*#" {
                case_fail $case_num
            }
            -re "#" {
                case_pass $case_num
                break
            }
            timeout {
                case_fail $case_num
            }
        }
    }

    incr case_num
    set timeout 1000
    send "date; time dd if=/dev/zero of=$mnt_point/10m.bin bs=1M count=10; date\r"
    while 1 {
        expect {
            -re "(\[eE]rror|\[fF]ail).*#" {
                case_fail $case_num
            }
            -re "#" {
                case_pass $case_num
                break
            }
            timeout {
                case_fail $case_num
            }
        }
    }

    incr case_num
    send "time cat $mnt_point/10m.bin > /dev/null\r"
    while 1 {
        expect {
            -re "real.*user.*sys.*#" {
                case_pass $case_num
                break
            }
            timeout {
                case_fail $case_num
            }
        }
    }

    incr case_num
    set timeout 5
    send "umount $mnt_point\r"
    while 1 {
        expect {
            -re "(umount:|\[eE]rror|\[fF]ail).*#" {
                case_fail $case_num
            }
            -re "#" {
                case_pass $case_num
                break
            }
            timeout {
                case_fail $case_num
            }
        }
    }
}


step "Start testing."
set case_num 0
set mnt_point "/mnt/usb"

if { $board_type == "BF548-EZKIT" } {
    set dev "/dev/sdb"
} else {
    set dev "/dev/sdc"
}

set timeout 5
sleep 3
send "ifconfig eth0 $targetip\r"
sleep 3
send "\r"
expect -re $kernel_prompt

incr case_num
set timeout 10
send "dd if=/dev/zero of=fsg.block bs=1M count=16\r"
while 1 {
    expect {
        -re "(\[eE]rror|\[fF]ail).*$kernel_prompt" {
            case_fail $case_num
        }
        -re "records in.*records out.*$kernel_prompt" {
            case_pass $case_num
            break
        }
        timeout {
            case_fail $case_num
        }
    }
}

incr case_num
set timeout 20
send "modprobe g_file_storage file=/fsg.block stall=0\r"
while 1 {
    expect {
        -re "(modprobe:|\[eE]rror|\[fF]ail).*$kernel_prompt" {
            case_fail $case_num
        }
        -re "File-backed Storage Gadget.*fsg\.block" {
            expect "speed config #1"
            break
        }
        timeout {
            case_fail $case_num
        }
    }
}
send "lsmod\r"
while 1 {
    expect {
        -re "(lsmod:|\[eE]rror|\[fF]ail).*$kernel_prompt" {
            case_fail $case_num
        }
        -re "g_file_storage.*$kernel_prompt" {
            case_pass $case_num
            break
        }
        timeout {
            case_fail $case_num
        }
    }
}

spawn /bin/bash
set host_spawn_id $spawn_id

incr case_num
set timeout 15
send "su -\r"
while 1 {
    expect {
        -re "\[pP]assword" {
            send "$password\r"
            continue
        }
        -re "\[eE]rror|\[fF]ail" {
            case_fail $case_num
        }
        -re "#" {
            case_pass $case_num
            break
        }
        timeout {
            case_fail $case_num
        }
    }
}

incr case_num
set timeout 5
send "mkdir -p $mnt_point\r"
while 1 {
    expect {
        -re "(mkdir:|\[eE]rror|\[fF]ail).*#" {
            case_fail $case_num
        }
        -re "#" {
            case_pass $case_num
            break
        }
        timeout {
            case_fail $case_num
        }
    }
}

sleep 10
incr case_num
set timeout 10
send "ls $dev\r"
while 1 {
    expect {
        -re "(ls:|\[eE]rror|\[fF]ail).*#" {
            case_fail $case_num
        }
        -re "#" {
            case_pass $case_num
            break
        }
        timeout {
            case_fail $case_num
        }
    }
}

incr case_num
set timeout 90
send "time fdisk $dev\r"
while 1 {
    expect {
        -re "(fdisk:|\[eE]rror|\[fF]ail).*#" {
            case_fail $case_num
        }
        -re "Command.*:" {
            send "n\r"
            expect -re "primary partition \\\(1-4\\\)"
            send "p\r"
            expect -re "Partition number \\\(1-4\\\):"
            send "1\r"
            expect -re "First cylinder.*:"
            send "\r"
            expect -re "Using default.*Last cylinder.*:"
            send "\r"
            expect -re "Using default.*Command.*:"
            send "w\r"
            while 1 {
                expect {
                    -re "kernel still uses the old table|\[eE]rror|\[fF]ail" {
                        case_fail $case_num
                    }
                    -re "Syncing disks" {
                        send "\r"
                        expect "#"
                        case_pass $case_num
                        break
                    }
                    timeout {
                        case_fail $case_num
                    }
                }
            }
            break
        }
        timeout {
            case_fail $case_num
        }
    }
}

append dev "1"

incr case_num
set timeout 600
send "mke2fs $dev\r"
while 1 {
    expect {
        -re "Writing inode tables.*done.*done.*#" {
            case_pass $case_num
            break
        }
        timeout {
            case_fail $case_num
        }
    }
}

incr case_num
set timeout 5
send "mount -t ext2 -o sync $dev $mnt_point\r"
while 1 {
    expect {
        -re "(mount:|\[eE]rror|\[fF]ail).*#" {
            case_fail $case_num
        }
        -re "#" {
            case_pass $case_num
            break
        }
        timeout {
            case_fail $case_num
        }
    }
}

incr case_num
set timeout 1000
send "date; time dd if=/dev/zero of=$mnt_point/10m.bin bs=1M count=10; date\r"
while 1 {
    expect {
        -re "(\[eE]rror|\[fF]ail).*#" {
            case_fail $case_num
        }
        -re "#" {
            case_pass $case_num
            break
        }
        timeout {
            case_fail $case_num
        }
    }
}

incr case_num
send "time cat $mnt_point/10m.bin > /dev/null\r"
while 1 {
    expect {
        -re "real.*user.*sys.*#" {
            case_pass $case_num
            break
        }
        timeout {
            case_fail $case_num
        }
    }
}

incr case_num
set timeout 5
send "ls -l $mnt_point\r"
while 1 {
    expect {
        -re "(\[eE]rror|\[fF]ail).*#" {
            case_fail $case_num
        }
        -re "10m\.bin.*lost\\+found.*#" {
            case_pass $case_num
            break
        }
        timeout {
            case_fail $case_num
        }
    }
}


incr case_num
set timeout 5
send "umount $mnt_point\r"
while 1 {
    expect {
        -re "(umount:|\[eE]rror|\[fF]ail).*#" {
            case_fail $case_num
        }
        -re "#" {
            case_pass $case_num
            break
        }
        timeout {
            case_fail $case_num
        }
    }
}

for {set count 1} {$count <= $Total_Count} {incr count} {
    standby_test_case
    send_user "\nStandby test case 1 round $count pass\n\n"
}

set spawn_id $host_spawn_id
sleep 3
send "ls $mnt_point\r"
expect "#"

incr case_num
set timeout 5
send "mount -t ext2 -o sync $dev $mnt_point\r"
while 1 {
    expect {
        -re "(mount:|\[eE]rror|\[fF]ail).*#" {
            case_fail $case_num
        }
        -re "#" {
            case_pass $case_num
                break
        }
        timeout {
            case_fail $case_num
        }
    }
}

incr case_num
set timeout 10
send "rm $mnt_point/10m.bin\r"
while 1 {
    expect {
        -re "(\[eE]rror|\[fF]ail).*#" {
            case_fail $case_num
        }
        -re "#" {
            case_pass $case_num
            break
        }
        timeout {
            case_fail $case_num
        }
    }
}

incr case_num
set timeout 5
send "ls -l $mnt_point/\r"
while 1 {
    expect {
        -re "(10m\.bin|\[eE]rror|\[fF]ail).*#" {
            case_fail $case_num
        }
        -re "#" {
            case_pass $case_num
            break
        }
        timeout {
            case_fail $case_num
        }
    }
}

incr case_num
set timeout 5
send "umount $mnt_point\r"
while 1 {
    expect {
        -re "(umount:|\[eE]rror|\[fF]ail).*#" {
            case_fail $case_num
        }
        -re "#" {
            case_pass $case_num
            break
        }
        timeout {
            case_fail $case_num
        }
    }
}


set spawn_id $kermit_spawn_id

incr case_num
set timeout 5
send "modprobe -r g_file_storage\r"
while 1 {
    expect {
        -re "(modprobe:|\[eE]rror|\[fF]ail).*$kernel_prompt" {
            case_fail $case_num
        }
        -re $kernel_prompt {
            case_pass $case_num
            break
        }
        timeout {
            case_fail $case_num
        }
    }
}

incr case_num
set timeout 5
send "lsmod\r"
while 1 {
    expect {
        -re "g_file_storage" {
            case_fail $case_num
        }
        -re $kernel_prompt {
            case_pass $case_num
            break
        }
        timeout {
            case_fail $case_num
        }
    }
}

all_pass
send_user "Ending $argv0\n"
log_file
