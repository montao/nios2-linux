#!/usr/bin/expect --

#
# Build Kernel for USB OTG test
#

source ../kernel_config.exp
log_file [log_file_name "$argv0"]
send_user "Starting $argv0\n"

set env(EXT2_BLOCKS) "16384"

cd $uclinux_path/testsuites
step "Make config."
source make_default_config.exp

cd $uclinux_path/testsuites
step "Config musb driver"
source make_config_musb.exp

cd $uclinux_path
set timeout 300
spawn make config
while 1 {
    expect {
        -re "Customize Kernel Settings.*DEFAULTS_KERNEL.*\\\[.*]" {
            send "y\r"
        }

        -re "Customize Application/Library Settings.*DEFAULTS_VENDOR.*\\\[.*]" {
            send "y\r"
        }

        -nocase -re "\\\[\[^\r]*] (\\\(NEW\\\) )?(\[ymn])|choice\\\[.*]: \[0-9]+\r" {
            continue
        }

        -re "Virtual terminal.*VT.*\\\[.*]" {
            send "N\r"
        }

#enable power Management, otherwise build will fail
        -re "Suspend to RAM and standby \\\(SUSPEND\\\) \\\[.*]" {
            send "Y\r"
        }

        # Select PM_RUNTIME to enable USB_SUSPEND
        -re "Run-time PM core functionality.*PM_RUNTIME.*\\\[.*]" {
            send "Y\r"
        }

#Config USB
        -re "USB Mass Storage support.*USB_STORAGE.*\\\[.*]" {
            send "Y\r"
        }

        -re "USB Peripheral Controller \\\[.*]" {
            send "Y\r"
        }

        -re "Platform Glue Layer \\\[.*]" {
            send "Y\r"
        }

        -re " (\[0-9]+). Inventra.*USB_INVENTRA_DMA.* (\[0-9]+). Disable DMA.*MUSB_PIO_ONLY.*\[cC]hoice.*\\\[.*]" {
            send "\r"
        }

        -re "USB Gadget Drivers \\\[.*]" {
            send "M\r"
        }

        -re "Gadget Zero.*USB_ZERO.*\\\[.*]" {
            send "M\r"
        }

#other disk related configs
        -re "SCSI device support \\\(SCSI\\\).*\\\[./././.]" {
            if { [string match "BF527-EZKIT*" $board_type] } {
                send "Y\r"
            } else {
                send "\r"
            }
        }

        -re "SCSI disk support.*BLK_DEV_SD.*\\\[./././.]" {
            send "Y\r"
        }

        -re "Second extended fs support.*EXT2_FS.*\\\[.*]" {
            send "Y\r"
        }

        -re "MSDOS fs support.*MSDOS_FS.*\\\[./././.]" {
            send "Y\r"
        }

        -re "VFAT.*Windows-95.*fs support.*VFAT_FS.*\\\[./././.]" {
            send "Y\r"
        }

        -re "Default codepage for FAT.*FAT_DEFAULT_CODEPAGE.*\\\[*]" {
            send "437\r"
        }

        -re "Default iocharset for FAT \\\(FAT_DEFAULT_IOCHARSET\\\).*\\\[.*]" {
            send "iso8859-1\r"
        }

        -re "Default NLS Option \\\(NLS_DEFAULT\\\).*\\\[.*]" {
            send "iso8859-1\r"
        }

        -re "Codepage 437.*United States, Canada.*NLS_CODEPAGE_437.*\\\[./././.]" {
            send "Y\r"
        }

        -re "NLS ISO 8859-1.*Latin 1; Western European Languages.*NLS_ISO8859_1.*\\\[./././.]" {
            send "Y\r"
        }

#Config applications
        -re "fdisk.*USER_FDISK_FDISK.*\\\[.*]" {
            send "Y\r"
        }

        -re "mke2fs.*USER_E2FSPROGS_MISC_MKE2FS.*\\\[.*]" {
            send "Y\r"
        }

        -re "e2fsck.*USER_E2FSPROGS_E2FSCK_E2FSCK.*\\\[.*]" {
            send "Y\r"
        }

        -re "mkdosfs.*USER_DOSFSTOOLS_MKDOSFS.*\\\[.*]" {
            send "Y\r"
        }

        -re "dosfsck.*USER_DOSFSTOOLS_DOSFSCK.*\\\[.*]" {
            send "Y\r"
        }

        -re "bonnie\\\+\\\+.*USER_BONNIE.*\\\[.*]" {
            send "y\r"
        }

        "\\\(*) \\\[*]" {
            sleep .01
            send "\r"
        }

        -re "\[cC]hoice\\\[.*]:" {
            send "\r"
        }

        eof {
            send_user "\nEnd of configuration\n"
            break
        }

        timeout {
            send_user "\n\nFATAL ERROR: config prompt timeout in make config\n\n"
            exit
        }
    }
}

cd $uclinux_path/testsuites
step "Make"
source make_kernel.exp

cd $uclinux_path/testsuites
step "Copy linux"
source copy_image.exp

send_user "Ending $argv0\n"
log_file
