#!/usr/bin/expect

source ../kernel_config.exp
source  ../board_info.exp
log_file [log_file_name "$argv0"]
send_user "Starting $argv0\n"

cd $uclinux_path/testsuites
step "Make config."
source  make_default_config.exp

cd $uclinux_path
set timeout 300
set baud_rate_done_flag 0
spawn make config
while 1 {
    expect {
        -re "Customize Kernel Settings.*DEFAULTS_KERNEL.*\\\[.*]" {
            send "y\r"
        }

        -re "Customize Application/Library Settings.*DEFAULTS_VENDOR.*\\\[.*]" {
            send "y\r"
        }

        -nocase -re "\\\[\[^\r]*] (\[ymn])|choice\\\[.*]: \[0-9]+\r" {
            continue
        }

        -re "I2C support \\\(I2C\\\).*\\\[.*]" {
            send "Y\r"
        }

        -re "Blackfin TWI I2C support.*I2C_BLACKFIN_TWI.*\\\[.*]" {
            send "Y\r"
        }

        -re "Support for generic platform NAND driver.*MTD_NAND_PLATFORM.*\\\[.*]" {
            #conflict with TFT LCD
            send "N\r"
        }

        -re "Backlight & LCD device support.*BACKLIGHT_LCD_SUPPORT.*\\\[.*]" {
            send "Y\r"
        }

        -re "Support for frame buffer devices.*FB.*\\\[.*]" {
            send "Y\r"
        }

        -re "SHARP LQ043 TFT LCD.*BF548 EZKIT.*\\\[.*]" {
            if { $board_type == "BF548-EZKIT" } {
                send "M\r"
            } else {
                send "N\r"
            }
        }

        -re "Varitronix COG-T350MCQB TFT LCD display.*FB_BFIN_T350MCQB.*\\\[.*]" {
            if { $board_type == "BF527-EZKIT" } {
                send "M\r"
            } else {
                send "N\r"
            }
        }

        -re "SHARP LQ035Q1DH02 TFT LCD.*FB_BFIN_LQ035Q1.*\\\[.*]" {
            if { $board_type == "BF527-EZKIT-V2" } {
                send "M\r"
            } else {
                send "N\r"
            }
        }

#LQ035 on BF537-STAMP
        -re "SPI controller driver for ADI Blackfin5xx.*SPI_BFIN.*\\\[.*]" {
            if { $board_type == "BF537-STAMP" } {
                send "M\r"
            } else {
                send "\r"
            }
        }

        -re "SHARP LQ035 TFT LCD.*FB_BF537_LQ035.*\\\[.*]" {
            if { $board_type == "BF537-STAMP" } {
                send "M\r"
            } else {
                send "N\r"
            }
        }

        -re "VIDEO test program.*USER_BLACKFIN_VIDEOTEST.*\\\[.*]" {
            send "Y\r"
        }

        -re "ThrustMaster devices support.*THRUSTMASTER_FF.*\\\[.*]" {
            send "N\r"
        }

        -re "Zeroplus based game controller support.*ZEROPLUS_FF.*\\\[.*]" {
            send "N\r"
        }

        "\\\(*) \\\[*]" {
            sleep .05
            send "\r"
        }

        -re "\[cC]hoice\\\[.*]:" {
            send "\r"
        }

        eof {
            puts "End of configuration"
            break
        }

        timeout {
            puts "\n\nFATAL ERROR: config prompt timeout in make config"
            break
        }
    }
}

cd $uclinux_path/testsuites
step "Make"
source  make_kernel.exp

cd $uclinux_path/testsuites
step "Copy linux"
source   copy_image.exp

send_user "Ending $argv0\n"
log_file
