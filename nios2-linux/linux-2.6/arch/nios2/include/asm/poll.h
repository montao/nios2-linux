#ifndef _ASM_NIOS2_POLL_H
#define _ASM_NIOS2_POLL_H

#define POLLWRNORM	POLLOUT
#define POLLWRBAND	256

#include <asm-generic/poll.h>

#endif
